﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateController : MonoBehaviour {

    public bool open = false;

    float openTimer = 1;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (open && openTimer > 0)
        {
            transform.Translate(-transform.worldToLocalMatrix.MultiplyVector(transform.right).normalized * Time.deltaTime * 10);
            openTimer -= Time.deltaTime;
        }
	}
}
