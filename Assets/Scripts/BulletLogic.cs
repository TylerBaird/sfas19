﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletLogic : MonoBehaviour
{

    // The speed of the rock
    [SerializeField]
    protected float m_RockSpeed = 15.0f;

    [SerializeField]
    GameObject audioSphere;
    float fadeTimer = 0;
    bool audioSphereActive = false;

    bool m_Active = true;

    // Use this for initialization
    void Start()
    {
        // Add velocity to the rock
        GetComponent<Rigidbody>().velocity = transform.forward * m_RockSpeed;
    }

    void Update()
    {
        if (!m_Active && !audioSphereActive)
        {
            audioSphere.SetActive(true);
            audioSphereActive = true;
        }
        if (!m_Active)
        {
            Color colour = audioSphere.GetComponent<Renderer>().material.color;
            Color colour2 = audioSphere.GetComponent<Renderer>().material.color;
            colour.a = 0.3f;
            colour2.a = 0;
            fadeTimer += Time.deltaTime / 2.0f;
            audioSphere.GetComponent<Renderer>().material.color = Color.Lerp(colour, colour2, fadeTimer);
            if (audioSphere.GetComponent<Renderer>().material.color == colour2)
                audioSphere.SetActive(false);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {

        if(m_Active && collision.gameObject.tag == "Enemy")
        {
            collision.gameObject.GetComponent<AIController>().stunned = true;
        }

        if(m_Active)
        {
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            GetComponent<Rigidbody>().useGravity = true;
            m_Active = false;
        } 
    }
}
