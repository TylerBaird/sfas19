﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{
    // --------------------------------------------------------------

    // The character's running speed
    [SerializeField]
    float m_MovementSpeed = 4.0f;

    // The gravity strength
    [SerializeField]
    float m_Gravity = 60.0f;

    // The maximum speed the character can fall
    [SerializeField]
    float m_MaxFallSpeed = 20.0f;

    // --------------------------------------------------------------

    // The charactercontroller of the player
    CharacterController m_CharacterController;

    // The current movement direction in x & z.
    Vector3 m_MovementDirection = Vector3.zero;

    // The current vertical / falling speed
    float m_VerticalSpeed = 0.0f;

    // The current movement offset
    Vector3 m_CurrentMovementOffset = Vector3.zero;

    // Whether the player is alive or not
    bool m_IsAlive = true;

    PlayerController m_PlayerController;
    Transform m_PlayerTransform;

    // --------------------------------------------------------------

    [SerializeField]
    bool rotating = true;

    [SerializeField]
    GameObject[] WayPoints;

    [SerializeField]
    int waypointIndex = 0;

    [SerializeField]
    float rotationTime = 5;

    [SerializeField]
    float rotationTimer;

    int waypointIndexInc = 1;

    [SerializeField]
    bool cycle;

    float step;

    public bool stunned = false;
    float stunnedTimer = 7;

    public bool spotted = false;

    [SerializeField]
    bool looking = false;
    [SerializeField]
    float lookingTimer = 5;

    GameObject lookingAt;


    // --------------------------------------------------------------

    void Awake()
    {
        m_CharacterController = GetComponent<CharacterController>();
    }

    // Use this for initialization
    void Start()
    {
        // Get Player information
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if(player)
        {
            m_PlayerController = player.GetComponent<PlayerController>();
            m_PlayerTransform = player.transform;
        }
        rotationTimer = rotationTime;
    }

    void ApplyGravity()
    {
        // Apply gravity
        m_VerticalSpeed -= m_Gravity * Time.deltaTime;

        // Make sure we don't fall any faster than m_MaxFallSpeed.
        m_VerticalSpeed = Mathf.Max(m_VerticalSpeed, -m_MaxFallSpeed);
        m_VerticalSpeed = Mathf.Min(m_VerticalSpeed, m_MaxFallSpeed);
    }

    // Update is called once per frame
    void Update()
    {
        if(spotted)
        {
            FindObjectOfType<PauseScript>().deadPopUp();
        }

        if (!stunned)
        {
            if (!looking)
            {
                if (rotating)
                {
                    if (rotationTimer > 0)
                    {
                        Vector3 targetDir = WayPoints[waypointIndex].transform.position - transform.position;
                        targetDir = new Vector3(targetDir.x, 0, targetDir.z);

                        step = Time.deltaTime;
                        Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0f);

                        transform.rotation = Quaternion.LookRotation(newDir);

                        rotationTimer -= Time.deltaTime;
                    }
                    if (rotationTimer <= 0)
                    {
                        if (cycle)
                        {
                            if (waypointIndex == WayPoints.Length - 1)
                                waypointIndex = 0;
                            else
                                waypointIndex += waypointIndexInc;
                        }
                        else
                        {

                            if (waypointIndex == WayPoints.Length - 1)
                                waypointIndexInc = -1;
                            if (waypointIndex == 0)
                                waypointIndexInc = 1;
                            waypointIndex += waypointIndexInc;
                        }
                        rotationTimer = rotationTime;
                    }
                }
                else
                {
                    if (rotationTimer > 0)
                    {
                        Vector3 targetDir = WayPoints[waypointIndex].transform.position - transform.position;
                        targetDir = new Vector3(targetDir.x, 0, targetDir.z);

                        step = Time.deltaTime;
                        Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0f);

                        transform.rotation = Quaternion.LookRotation(newDir);

                        rotationTimer -= Time.deltaTime;
                    }
                    if (rotationTimer <= 0)
                    {
                        step = 10 * Time.deltaTime;
                        transform.position = Vector3.MoveTowards(transform.position, new Vector3(WayPoints[waypointIndex].transform.position.x, 1.032f, WayPoints[waypointIndex].transform.position.z), step);
                        if (Vector3.Distance(new Vector3(WayPoints[waypointIndex].transform.position.x, 0, WayPoints[waypointIndex].transform.position.z), new Vector3(transform.position.x, 0, transform.position.z)) < 0.4)
                        {
                            transform.position = new Vector3(WayPoints[waypointIndex].transform.position.x, 1.032f, WayPoints[waypointIndex].transform.position.z);
                        }
                    }
                    if (transform.position == new Vector3(WayPoints[waypointIndex].transform.position.x, 1.032f, WayPoints[waypointIndex].transform.position.z))
                    {
                        if (cycle)
                        {
                            if (waypointIndex == WayPoints.Length - 1)
                                waypointIndex = 0;
                            else
                                waypointIndex += waypointIndexInc;
                        }
                        else
                        {

                            if (waypointIndex == WayPoints.Length - 1)
                                waypointIndexInc = -1;
                            if (waypointIndex == 0)
                                waypointIndexInc = 1;
                            waypointIndex += waypointIndexInc;
                        }
                        rotationTimer = rotationTime;
                    }
                }
            }
            else
            {
                Vector3 targetDir = lookingAt.GetComponentInParent<Transform>().position - transform.position;
                targetDir = new Vector3(targetDir.x, 0, targetDir.z);

                step = Time.deltaTime;
                Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0f);

                transform.rotation = Quaternion.LookRotation(newDir);

                lookingTimer -= Time.deltaTime;

                if (lookingTimer <= 0)
                {
                    looking = false;
                    lookingTimer = 5;
                }
            }
        }
        else
        {
            transform.GetChild(2).gameObject.SetActive(false);
            stunnedTimer -= Time.deltaTime;
            if(stunnedTimer <= 0)
            {
                stunned = false;
                stunnedTimer = 7;
                transform.GetChild(2).gameObject.SetActive(true);
            }
        }


        // Update jumping input and apply gravity
        ApplyGravity();

        // Calculate actual motion
        m_CurrentMovementOffset = (m_MovementDirection * m_MovementSpeed + new Vector3(0, m_VerticalSpeed, 0)) * Time.deltaTime;

        // Move character
        m_CharacterController.Move(m_CurrentMovementOffset);

        // Rotate the character in movement direction
        if(m_MovementDirection != Vector3.zero)
        {
            RotateCharacter(m_MovementDirection);
        }
    }

    float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
    {
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
    }

    void RotateCharacter(Vector3 movementDirection)
    {
        Quaternion lookRotation = Quaternion.LookRotation(movementDirection);
        if (transform.rotation != lookRotation)
        {
            transform.rotation = lookRotation;
        }
    }

    public void Die()
    {
        m_IsAlive = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "audioSphere")
        {
            looking = true;

            lookingAt = other.gameObject;
        }
    }
}
