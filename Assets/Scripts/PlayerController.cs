﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    // --------------------------------------------------------------

    // The character's running speed
    [SerializeField]
    float m_RunSpeed = 5.0f;

    // The gravity strength
    [SerializeField]
    float m_Gravity = 60.0f;

    // The maximum speed the character can fall
    [SerializeField]
    float m_MaxFallSpeed = 20.0f;

    // --------------------------------------------------------------

    // The charactercontroller of the player
    CharacterController m_CharacterController;

    // The current movement direction in x & z.
    Vector3 m_MovementDirection = Vector3.zero;

    // The current movement speed
    float m_MovementSpeed = 0.0f;

    // The current vertical / falling speed
    float m_VerticalSpeed = 0.0f;

    // The current movement offset
    Vector3 m_CurrentMovementOffset = Vector3.zero;

    // --------------------------------------------------------------

    [SerializeField]
    bool hasRock = true;

    [SerializeField]
    GameObject rockPrefab;

    [SerializeField]
    GameObject rockSpawnPoint;

    [SerializeField]
    GameObject AudioSphere;
    float fadeTimer = 0;

    // --------------------------------------------------------------

    void Awake()
    {
        m_CharacterController = GetComponent<CharacterController>();
    }

    void ApplyGravity()
    {
        // Apply gravity
        m_VerticalSpeed -= m_Gravity * Time.deltaTime;

        // Make sure we don't fall any faster than m_MaxFallSpeed.
        m_VerticalSpeed = Mathf.Max(m_VerticalSpeed, -m_MaxFallSpeed);
        m_VerticalSpeed = Mathf.Min(m_VerticalSpeed, m_MaxFallSpeed);
    }

    void UpdateMovementState()
    {
        // Get Player's movement input and determine direction and set run speed
        float horizontalInput = Input.GetAxisRaw("Horizontal_P1");
        float verticalInput = Input.GetAxisRaw("Vertical_P1");

        m_MovementDirection = new Vector3(horizontalInput, 0, verticalInput);
        m_MovementSpeed = m_RunSpeed;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetButtonDown("Fire1") && hasRock)
        {
            Instantiate(rockPrefab, rockSpawnPoint.transform.position, transform.rotation);
            hasRock = false;
        }

        // Update movement input
        UpdateMovementState();

        if(Input.GetAxisRaw("Horizontal_P1") != 0 || Input.GetAxisRaw("Vertical_P1") != 0)
        {
            AudioSphere.SetActive(true);
            Color colour = AudioSphere.GetComponent<Renderer>().material.color;
            colour.a = 0.3f;
            AudioSphere.GetComponent<Renderer>().material.color = colour;
            fadeTimer = 0;
        }
        else
        {
            Color colour = AudioSphere.GetComponent<Renderer>().material.color;
            Color colour2 = AudioSphere.GetComponent<Renderer>().material.color;
            colour.a = 0.3f;
            colour2.a = 0;
            fadeTimer += Time.deltaTime / 2.0f;
            AudioSphere.GetComponent<Renderer>().material.color = Color.Lerp(colour, colour2, fadeTimer);
            if(AudioSphere.GetComponent<Renderer>().material.color == colour2)
            {
                AudioSphere.SetActive(false);
                fadeTimer = 0;
            }
        }

        ApplyGravity();

        // Calculate actual motion
        m_CurrentMovementOffset = (m_MovementDirection * m_MovementSpeed + new Vector3(0, m_VerticalSpeed, 0)) * Time.deltaTime;

        // Move character
        m_CharacterController.Move(m_CurrentMovementOffset);

        // Rotate the character towards the mouse cursor
        RotateCharacterTowardsMouseCursor();
    }

    float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
    {
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
    }

    void RotateCharacter(Vector3 movementDirection)
    {
        Quaternion lookRotation = Quaternion.LookRotation(movementDirection);
        if (transform.rotation != lookRotation)
        {
            transform.rotation = lookRotation;
        }
    }

    void RotateCharacterTowardsMouseCursor()
    {
        Vector3 mousePosInScreenSpace = Input.mousePosition;
        Vector3 playerPosInScreenSpace = Camera.main.WorldToScreenPoint(transform.position);
        Vector3 directionInScreenSpace = mousePosInScreenSpace - playerPosInScreenSpace;

        float angle = Mathf.Atan2(directionInScreenSpace.y, directionInScreenSpace.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(-angle + 90.0f, Vector3.up);
    }

    private void OnTriggerEnter(Collider coll)
    {
        if(coll.tag == "Rock")
        {
            Destroy(coll.gameObject);
            hasRock = true;
        }

        if(coll.tag == "Button")
        {
            coll.GetComponent<ButtonController>().triggered = true;
        }

        if(coll.tag == "LoS")
        {
            coll.GetComponentInParent<AIController>().spotted = true;
        }

        if(coll.tag == "Exit")
        {
            FindObjectOfType<PauseScript>().WinPopUp();
        }
    }

}
