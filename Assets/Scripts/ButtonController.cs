﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonController : MonoBehaviour {

    [SerializeField]
    GameObject Gate;

    [SerializeField]
    public bool triggered = false;
	
	// Update is called once per frame
	void Update () {
		if(triggered)
        {
            GetComponent<Renderer>().material.color = Color.green;
            Gate.GetComponent<GateController>().open = true;
        }
	}
}
