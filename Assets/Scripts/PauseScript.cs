﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
//using UnityStandardAssets.Characters.FirstPerson;

public class PauseScript : MonoBehaviour
{

    bool isPaused = false;
    [SerializeField]
    GameObject overlay;

    [SerializeField]
    Button resumeBtn;
    [SerializeField]
    Button restartBtn;
    [SerializeField]
    Text endGameTxt;

    private void Start()
    {
        //Resume();
        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPaused == true)
            {
                FindObjectOfType<PlayerController>().enabled = true;
                isPaused = !isPaused;
                Time.timeScale = 1;
                overlay.SetActive(false);

            }
            else
            {
                FindObjectOfType<PlayerController>().enabled = false;
                isPaused = !isPaused;
                Time.timeScale = 0;
                overlay.SetActive(true);
                endGameTxt.text = "Paused";
                resumeBtn.gameObject.SetActive(true);
                restartBtn.gameObject.SetActive(false);
            }
        }
    }

    public void Resume()
    {
        FindObjectOfType<PlayerController>().enabled = true;
        isPaused = !isPaused;
        Time.timeScale = 1;
        overlay.SetActive(false);
    }

    public void deadPopUp()
    {
        FindObjectOfType<PlayerController>().enabled = false;
        resumeBtn.gameObject.SetActive(false);
        restartBtn.gameObject.SetActive(true);
        endGameTxt.enabled = true;
        endGameTxt.text = "You were spotted! Try Again!";
        isPaused = !isPaused;
        Time.timeScale = 0;
        overlay.SetActive(true);
    }

    public void WinPopUp()
    {
        FindObjectOfType<PlayerController>().enabled = false;
        resumeBtn.gameObject.SetActive(false);
        restartBtn.gameObject.SetActive(true);
        endGameTxt.enabled = true;
        endGameTxt.text = "You won the game, congratulations!";
        isPaused = !isPaused;
        Time.timeScale = 0;
        overlay.SetActive(true);
    }

    public void restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
